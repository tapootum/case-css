-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 08, 2017 at 04:06 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `thesis`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `ID` int(11) NOT NULL,
  `PRODUCT_NAME` text COLLATE utf8_unicode_ci NOT NULL,
  `S` double NOT NULL,
  `H` float NOT NULL,
  `C` int(11) NOT NULL,
  `MAX` int(11) NOT NULL,
  `MIN` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`ID`, `PRODUCT_NAME`, `S`, `H`, `C`, `MAX`, `MIN`) VALUES
(1, 'TF 1 Phase 30kVA 19kV', 10571, 0.02, 28027, 10, 3),
(2, 'TF 1 Phase 30kVA 22kV', 573.64, 0.0166667, 28412, 20, 10),
(3, 'TF 3 Phase 50kVA 22kV', 573.64, 0.0166667, 45050, 10, 2),
(4, 'TF 3 Phase 250kVA 22kV', 573.64, 0.0166667, 118040, 7, 2),
(5, 'Drop out Fuse cutouts 33kV 100A 8kA', 573.64, 0.0166667, 1132, 400, 200),
(6, 'Low tension fuse switch 1x400A 500V', 573.64, 0.0166667, 390, 800, 400),
(7, 'LED Indoor Lighting - LED TUBE 18W', 573.64, 0.0166667, 440, 0, 0),
(8, '3 Phase Power Capacitor', 573.64, 0.0166667, 2184, 0, 0),
(9, 'Voltage Transformer (Oil Type)', 573.64, 0.0166667, 15200, 0, 0),
(10, 'Current Transformer TYPE COL (Oil Type)', 573.64, 0.0166667, 10160, 0, 0),
(11, 'High Voltage Surge Arrester - 21kV', 573.64, 0.0166667, 1700, 800, 400),
(12, 'High Voltage Surge Arrester - 24kV', 573.64, 0.0166667, 1200, 800, 400),
(13, 'SF6 Gas Load Break Switch', 573.64, 0.0166667, 164560, 0, 0),
(14, 'Disconnecting Switch', 573.64, 0.0166667, 4232, 30, 24);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
