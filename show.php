<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="img/favicon.png" />
    <link rel="stylesheet" href="build/css-mint.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 300;
    }
    </style>
</head>

<body style="padding: 30px 30px 50px;">
    <header class="header">
        <div class="logo">
            <a href="index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header><br><br>

    <?php
session_start();
include 'db/db_conn.php';
include 'sm_cal.php';
include 'function.php';
$product_name_vc = $_SESSION["product_name_vc"];
$productData     = $_SESSION["productData2"];
$dBar_x          = $_SESSION["dBar_x"];
$estvard         = $_SESSION["estvard"];
$vcCal           = $_SESSION["vcCal"];
$productData2    = $productData;
?>
    <br><br>
    <ul class="breadcrumb">
        <li><a href="index.php">หน้าแรก</a></li>
        <li><a href="excel-upload.php">เลือกวิธีการสั่งซื้อ</a></li>
        <li><a href="menucal.php">เปรียบเทียบวิธีการสั่งซื้อ</a></li>
        <li><a href="#"><?php
echo $_GET['name'];
?></a></li>
    </ul>
    <?php

echo '<br><h4><font face="verdana">' . "ชื่อสินค้า : $product_name_vc" . '</font></h4>';

echo '<center><table style="width:100%;" border=1 class="info">';

$highestRow    = $_SESSION["highestRow_PN"];
$highestColumn = $_SESSION["highestColumn_PN"];

$PRODUCT_COUNT = 1;
$xx            = 1;
$sum_data1     = 0;
$sum_data2     = 0;

$product_names = $product_name_vc;
$rowData       = $productData2;
$MOUNT_COUNT   = 1;
for ($i = 0; $i < 12; $i++) {
    $sum_data1 = $sum_data1 + $productData2[$i];
    $sum_data2 = $sum_data2 + ($productData2[$i] * $productData2[$i]);
}
$productData = $rowData;
$dBar_x      = dBar($sum_data1, 12);
$estvard     = est_var_d($sum_data1, $sum_data2, 12);
$vcOriginal  = vc($sum_data1, $sum_data2, 12);
$tabletd = [ "ปริมาณการสั่ง (หน่วย)","สินค้าคงคลังต้นงวด (หน่วย)","สินค้าคงคลังปลายงวด (หน่วย)","สินค้าคงคลังถัวเฉลี่ย (หน่วย)","ค่าใช้จ่ายในการถือครอง (บาท)","ค่าใช้จ่ายในการสั่ง (บาท)"];
$tabletd2 = [ "ความต้องการ (หน่วย)","ปริมาณการสั่ง (หน่วย)","สินค้าคงคลังต้นงวด (หน่วย)","สินค้าคงคลังปลายงวด (หน่วย)","สินค้าคงคลังถัวเฉลี่ย (หน่วย)","ค่าใช้จ่ายในการถือครอง (บาท)","ค่าใช้จ่ายในการสั่ง (บาท)"];
if ($_GET['name'] == "วิธีปัจจุบัน") {
   $TORIGIN = $_SESSION["ORIGINAL"];
       $sum_origin1 = 0;
        $sum_origin2 = [0,0,0,0,0,0,0,0];
    echo '<tr><th align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">เดือนที่</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">' . $r . '</td>';
    }
    echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">รวม</td>';
    echo '</tr>';
    echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">ความต้องการ (หน่วย)</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center">' . $TORIGIN[12][$r - 1] . '</td>';
                $sum_origin1 += $TORIGIN[12][$r - 1];
    }
    echo '<td><center>' .$sum_origin1.'</td>';
    echo '</tr>';

    for ($e = 2; $e <= 7; $e++) {
       echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">'. $tabletd[$e - 2].'</th>';
          for ($r = 1; $r <= 12; $r++) {
            if ($e == 6 || $e == 7) {
                echo '<td align="center">' . number_format(round($TORIGIN[$e][$r], 2), 2, '.', ',') . '</td>';
            } else if ($e == 2){
                echo '<td align="center"><font color="#2edb25">' . $TORIGIN[$e][$r] . '</td>';
            } else {
                echo '<td align="center">' . $TORIGIN[$e][$r] . '</td>';
            }
                        $sum_origin2[$e] += $TORIGIN[$e][$r];
        }
        if ($e == 6 || $e == 7) {
                echo '<td><center>'.number_format(round($sum_origin2[$e], 2), 2, '.', ',').'</td>';
                $sum_origin3 += $sum_origin2[$e];
        } else if ($e == 2 ){
                echo '<td><center>'.$sum_origin2[$e].'</td>';
            } else {
        echo '<td></td>';
            }
        echo '</tr>';
    }
    echo '<tr><td colspan=13 align="right"><b>ต้นทุนสินค้าคงคลังโดยรวม (บาท)</td><td><center><b>'.number_format(round($sum_origin3, 2), 2, '.', ',').'</td></tr>';
} 




if ($_GET['name'] == "EOQ") {
    $TEOQ = $_SESSION["EOQ"];
        $sum_eoq1 = 0;
        $sum_eoq2 = [0,0,0,0,0,0,0,0];
    echo '<tr><th align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">เดือนที่</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">' . $r . '</td>';
    }
    echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">รวม</td>';
    echo '</tr>';
    echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">ความต้องการ (หน่วย)</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center">' . $TEOQ[12][$r - 1] . '</td>';
                $sum_eoq1 += $TEOQ[12][$r - 1];
    }
    echo '<td><center>' .$sum_eoq1.'</td>';
    echo '</tr>';

    for ($e = 2; $e <= 7; $e++) {
       echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">'. $tabletd[$e - 2].'</th>';
          for ($r = 1; $r <= 12; $r++) {
            if ($e == 6 || $e == 7) {
                echo '<td align="center">' . number_format(round($TEOQ[$e][$r], 2), 2, '.', ',') . '</td>';
            } else if ($e == 2){
                echo '<td align="center"><font color="#2edb25">' . $TEOQ[$e][$r] . '</td>';
            } else {
                echo '<td align="center">' . $TEOQ[$e][$r] . '</td>';
            }
                        $sum_eoq2[$e] += $TEOQ[$e][$r];
        }
        if ($e == 6 || $e == 7) {
                echo '<td><center>'.number_format(round($sum_eoq2[$e], 2), 2, '.', ',').'</td>';
                $sum_eoq3 += $sum_eoq2[$e];
        } else if ($e == 2 ){
                echo '<td><center>'.$sum_eoq2[$e].'</td>';
            } else {
        echo '<td></td>';
            }
        echo '</tr>';
    }
    echo '<tr><td colspan=13 align="right"><b>ต้นทุนสินค้าคงคลังโดยรวม (บาท)</td><td><center><b>'.number_format(round($sum_eoq3, 2), 2, '.', ',').'</td></tr>';
}
if ($_GET['name'] == "POQ") {
    $TPOQ = $_SESSION["POQ"];
        $sum_poq1 = 0;
        $sum_poq2 = [0,0,0,0,0,0,0,0];
    echo '<tr><th align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">เดือนที่</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">' . $r . '</td>';
    }
    echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">รวม</td>';
    echo '</tr>';
    echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">ความต้องการ (หน่วย)</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center">' . $TPOQ[12][$r - 1] . '</td>';
                $sum_poq1 += $TPOQ[12][$r - 1];
    }
    echo '<td><center>' .$sum_poq1.'</td>';
    echo '</tr>';

    for ($e = 2; $e <= 7; $e++) {
       echo '<tr><th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">'. $tabletd[$e - 2].'</th>';
          for ($r = 1; $r <= 12; $r++) {
            if ($e == 6 || $e == 7) {
                echo '<td align="center">' . number_format(round($TPOQ[$e][$r], 2), 2, '.', ',') . '</td>';
            } else if ($e == 2){
                echo '<td align="center"><font color="#2edb25">' . $TPOQ[$e][$r] . '</td>';
            } else { 
                echo '<td align="center">' . $TPOQ[$e][$r] . '</td>';
            }
                        $sum_poq2[$e] += $TPOQ[$e][$r];
        }
        if ($e == 6 || $e == 7) {
                echo '<td><center>'.number_format(round($sum_poq2[$e], 2), 2, '.', ',').'</td>';
                $sum_poq3 += $sum_poq2[$e];
        } else if ($e == 2 ){
                echo '<td><center>'.$sum_poq2[$e].'</td>';
            } else {
        echo '<td></td>';
            }
        echo '</tr>';
    }
    echo '<tr><td colspan=13 align="right"><b>ต้นทุนสินค้าคงคลังโดยรวม (บาท)</td><td><center><b>'.number_format(round($sum_poq3, 2), 2, '.', ',').'</td></tr>';
}

if ($_GET['name'] == "SM") {
    $TSM = $_SESSION["SM"];
	$sum_sm = [0,0,0,0,0,0,0];
    echo '<tr><th align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">เดือนที่</th>';
    for ($r = 1; $r <= 12; $r++) {
         echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">' . $r . '</td>';
    }
    echo '<td align="center" bgcolor="#5C9DED" ><font color="#FFFFFF">รวม</td>';
    echo '</tr>';




        for ($j = 0; $j <= 6; $j++) {
         echo '<th align="left" bgcolor="#5C9DED" ><font color="#FFFFFF">'. $tabletd2[$j].'</th>';
          for ($i = 0; $i < 12; $i++) {
            if ($j == 5 || $j == 6) {
                echo '<td align="center">' . number_format(round($TSM[4][$j][$i], 2), 2, '.', ',') . '</td>';
            } else if ($j == 1) {
                echo '<td align="center"><font color="#2edb25">' . $TSM[4][$j][$i]. '</td>';
            } else {
                echo '<td align="center">' . $TSM[4][$j][$i] . '</td>';
            }
			$sum_sm[$j] += $TSM[4][$j][$i];
        }
	 if ($j == 5 || $j == 6) {
            echo '<td align="center">' . number_format(round($sum_sm[$j], 2), 2, '.', ',') . '</td>';
                $sum_sm3 += $sum_sm[$j];
        } else if ($j == 0 || $j == 1) {
            echo '<td align="center">' . $sum_sm[$j]. '</td>';
        } else {
            echo '<td align="center"></td>';
        }
        echo '</tr>';
    }
    echo '<tr><td colspan=13 align="right"><b>ต้นทุนสินค้าคงคลังโดยรวม (บาท)</td><td><b><center>'.number_format(round($sum_sm3, 2), 2, '.', ',').'</td></tr>';
}

echo '</table>';
if ($_GET['name'] == "WW") {


    
}
?>
        <br><br>
        <br><br>
    <center>
    <img src="img/Transformer.jpg" height="120">
    <img src="img/VoltageTransformer.jpg" height="120">
    <img src="img/FuseLink.png" height="120">
    <img src="img/FuseCutout.jpg" height="120">
    <img src="img/LowTension.jpg" height="120">
    <img src="img/StreetLighting20W.jpg" height="120">
    <img src="img/CurrentTransformer.jpg" height="120">


</body>
</html>
