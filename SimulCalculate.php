<?php
  require './Const.php';
  require './SimulData.php';
  require './SimulMin.php';

  class SimulCalculate {
    private $simulInput;

    public function __construct(SimulInput $simulInput) {
      $this->simulInput = $simulInput;
    }

    public function calcSimul() {
      $simuls = $minSimuls = [];
      $minSimul = 0;
      #define(c,"");
      #define(s,"");
      for ($row = 0; $row < $GLOBALS['MONTH']; $row++) {
        $simuls[$row] = new SimulData();
        for ($month = $row; $month < $GLOBALS['MONTH']; $month++) {
          $qtys = $this->calcQty($row, $month);
          $inventories = $this->calcBeginAndEndInventory($qtys);
          $avgInv = $this->calcAvgInventory($inventories[0], $inventories[1]);
          $holdingCost = $this->calcHoldingCost($c, $avgInv);
          $orderCost = $this->calcOrderCost($s, $qtys);

          $simuls[$row]->addSimulByMonth($month, $this->calcSimulByMonth($avgInv, $minSimul, $row, $month));
        }

        $minSimuls[$row] = $this->calcMinSimul($simuls, $row);
        $minSimul = $minSimuls[$row]->getSimul();
      }

      return [$simuls, $minSimuls];
    }

    private function calcSimulByMonth($avgInvs, $minSimul, $monthStart, $monthEnd) {
      $simul = 0;

      foreach ($avgInvs as $index => $avgInv) {
        if($index >= $monthStart && $index <= $monthEnd) {
          $simul += $avgInv;
        }
      }

      return round($this->simulInput->getS() + $minSimul + ($this->simulInput->getHCalc() * $simul));
    }

    private function calcQty($monthStart, $monthEnd) {
      $demands = $this->simulInput->getDemands();
      $qtys = $demands;// copy demand into qty

      for($month = $monthStart; $month <= $monthEnd; $month++) {
        $qtys[$month] = 0;
        $qtys[$monthStart] += $demands[$month];
      }

      return $qtys;
    }

    public function calcBeginAndEndInventory($qtys) {
      $demands = $this->simulInput->getDemands();
      $beginInv = $endInv = [];

      for($month = 0; $month < $GLOBALS['MONTH']; $month++) {
        $beginInv[$month] = ($month == 0) ? $qtys[$month] : $endInv[$month - 1] + $qtys[$month];
        $endInv[$month] = $beginInv[$month] - $demands[$month];
      }

      return [$beginInv, $endInv];
    }

    public function calcAvgInventory($beginInv, $endInv) {
      $avgInv = [];

      for($month = 0; $month < $GLOBALS['MONTH']; $month++) {
        $avgInv[$month] = ($beginInv[$month] + $endInv[$month]) / 2;
      }

      return $avgInv;
    }

    public function calcHoldingCost($c, $avgInvs) {
      $holdingCosts = [];

      for($month = 0; $month < $GLOBALS['MONTH']; $month++) {
        $holdingCosts[$month] = round($avgInvs[$month] * (0.2 / $GLOBALS['MONTH']) * $c);
      }

      return $holdingCosts;
    }

    public function calcOrderCost($s, $qtys) {
      $orderCosts = [];

      for($month = 0; $month < $GLOBALS['MONTH']; $month++) {
        $orderCosts[$month] = $qtys[$month] > 0? round($s, 2): 0;
      }

      return $orderCosts;
    }

    private function calcMinSimul($simuls, $month) {
      $index = 0;
      $min = 999999999999999999999;// maximum of simul data

      foreach($simuls as $indexSimul => $simul) {
        $simulMonths = $simul->getSimuls();

        if($simulMonths[$month] < $min) {
          $index = $indexSimul;
          $min = $simulMonths[$month];
        }
      }

      return new SimulMin($index, $min);
    }
  }