<!DOCTYPE html>
<html lang="en">
<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
</head>
<body>
<h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1>
    <h3>เลือกสินค้า</h3>
    <form enctype="multipart/form-data" action="excel-upload.php" method="post" >
    <?php
        session_start();
        include 'db/db_conn.php';
        $sql = "SELECT * from product";
        $result = $conn->query($sql);
        echo '<select name="PRODNM">';
        if ($result->num_rows > 0) {
    // output data of each row
            while($row = $result->fetch_assoc()) {
                echo '<option value="' . $row["PRODUCT_NAME"].'">'.$row["PRODUCT_NAME"].'</option>';
            }
        } else {
            echo "0 results";
        }
        $conn->close();
    ?>
        <option>Here is the first option</option>
        </select><br><br>
        <h4>ข้อมูลยอดขายจริงรายเดือนย้อนหลัง (.xlsx only)</h4>
        *** จะต้องเป็นข้อมูลยอดขายจริง รายเดือน ย้อนหลัง จำนวน 12-36 เดือนล่าสุด
        <br><br>
            <input class="btn info line" type="file" name="filevc" id="filevc" required />
        <br><br>
        <h4>ข้อมูลความต้องการที่พยากรณ์ไว้ (.xlsx only)</h4>
            <input class="btn info line" type="file" name="file" id="file" required />
        <br><br>
        <input type="submit" value="Submit" />
    </form>
</body>
</html>