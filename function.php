<?php
function dBar($d_bar_y, $mount_counts)
{
    return $d_bar_y / $mount_counts;
}
function est_var_d($d_bar_x, $evd_x, $mount_counts)
{
    $dBar_s  = dBar($d_bar_x, $mount_counts);
    $estdvar = ($evd_x / $mount_counts) - ($dBar_s * $dBar_s);
    return $estdvar;
}
function vc($d_bar_z, $evd_z, $mount_counts)
{
    $dBar_a  = dBar($d_bar_z, $mount_counts);
    $estdvar = est_var_d($d_bar_z, $evd_z, $mount_counts);
    $vc_s    = $estdvar / ($dBar_a * $dBar_a);
    return $vc_s;
}
function quantity($dbar_q, $S, $H, $C)
{
    $Q = sqrt((2 * $dBar_q * $S) / ($H * $C));
    return $Q;
}
function original($product_name, $rawDatas)
{
    include 'db/db_conn.php';
    $sql     = "SELECT * FROM product";
    $resultx = $conn->query($sql);
    if ($resultx->num_rows > 0) {
        // output data of each row
        while ($rowO = $resultx->fetch_assoc()) {
            if ($rowO["PRODUCT_NAME"] == $product_name) {
                $S   = $rowO["S"];
                $h   = $rowO["H"];
                $C   = $rowO["C"];
                $MIN = $rowO["MIN"];
            }
        }
    } else {
        echo "0 results";
        exit;
    }
    $conn->close();
    $N = 1;
    for ($i = 0; $i <= 12; $i++) {
        $Demand[$i]              = 0;
        $Quantity[$i]            = 0;
        $Beginning_Inventory[$i] = 0;
        $Ending_Inventory[$i]    = 0;
        $Average_Inventory[$i]   = 0;
        $Holding_Cost[$i]        = 0;
        $Ordering_Cost[$i]       = 0;
        $sum_Holding_Cost[$i]    = 0;
        $sum_Ordering_Cost[$i]   = 0;
    }
    $COUNT_DATA = 1;
    for ($i = 0; $i < 12; $i++) {
        $Y = $rawDatas[$i];
        if ($COUNT_DATA < 13) {
            if ($COUNT_DATA <= "1") {
                $Demand[$COUNT_DATA]              = $Y;
                $Quantity[$COUNT_DATA]            = $Demand[$COUNT_DATA] + $MIN;
                $Beginning_Inventory[$COUNT_DATA] = $Quantity[$COUNT_DATA];
                $Ending_Inventory[$COUNT_DATA]    = $Beginning_Inventory[$COUNT_DATA] - $Demand[$COUNT_DATA];
                $Average_Inventory[$COUNT_DATA]   = ($Beginning_Inventory[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA]) / 2;
                $Holding_Cost[$COUNT_DATA]        = round($Average_Inventory[$COUNT_DATA] * (0.2 / 12) * $C, 2);
                
                
                if ($Quantity[$COUNT_DATA] > 0) {
                    $Ordering_Cost[$COUNT_DATA] = round($S, 2);
                } else {
                    $Ordering_Cost[$COUNT_DATA] = 0;
                }
                $sum_Holding_Cost[$COUNT_DATA]  = $sum_Holding_Cost[$COUNT_DATA] + $Holding_Cost[$COUNT_DATA];
                $sum_Ordering_Cost[$COUNT_DATA] = $sum_Ordering_Cost[$COUNT_DATA] + $Ordering_Cost[$COUNT_DATA];
                
            } else {
                $Demand[$COUNT_DATA] = $Y;
                
                if ($Demand[$COUNT_DATA] >= $Ending_Inventory[$COUNT_DATA - 1] || $Demand[$COUNT_DATA] <= $MIN) {
                    $Quantity[$COUNT_DATA] = $Demand[$COUNT_DATA];
                } else {
                    $Quantity[$COUNT_DATA] = 0;
                }
                
                $Beginning_Inventory[$COUNT_DATA] = $Quantity[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA - 1];
                $Ending_Inventory[$COUNT_DATA]    = $Beginning_Inventory[$COUNT_DATA] - $Demand[$COUNT_DATA];
                $Average_Inventory[$COUNT_DATA]   = ($Beginning_Inventory[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA]) / 2;
                $Holding_Cost[$COUNT_DATA]        = round($Average_Inventory[$COUNT_DATA] * (0.2 / 12) * $C, 2);
                
                if ($Quantity[$COUNT_DATA] > 0) {
                    $Ordering_Cost[$COUNT_DATA] = round($S, 2);
                } else {
                    $Ordering_Cost[$COUNT_DATA] = 0;
                }
                $sum_Holding_Cost[$COUNT_DATA]  = $sum_Holding_Cost[$COUNT_DATA] + $Holding_Cost[$COUNT_DATA];
                $sum_Ordering_Cost[$COUNT_DATA] = $sum_Ordering_Cost[$COUNT_DATA] + $Ordering_Cost[$COUNT_DATA];
            }
        }
        $COUNT_DATA++;
    }
    $Total_Cost         = 0;
    $sum_Ordering_CostX = 0;
    $sum_Holding_CostX  = 0;
    for ($i = 1; $i <= 12; $i++) {
        $Total_Cost         = $Total_Cost + $sum_Holding_Cost[$i] + $sum_Ordering_Cost[$i];
        $sum_Ordering_CostX = $sum_Ordering_CostX + $sum_Ordering_Cost[$i];
        $sum_Holding_CostX  = $sum_Holding_CostX + $sum_Holding_Cost[$i];
    }
    $ORI_DATA = array(
        1 => $Total_Cost,
        2 => $Quantity,
        3 => $Beginning_Inventory,
        4 => $Ending_Inventory,
        5 => $Average_Inventory,
        6 => $Holding_Cost,
        7 => $Ordering_Cost,
        8 => $sum_Holding_Cost,
        9 => $sum_Ordering_Cost,
        10 => $sum_Ordering_CostX,
        11 => $sum_Holding_CostX,
        12 => $rawDatas
    );
    
    return $ORI_DATA;
}


function EOQ($product_name, $rawDatas, $dBarEOQ, $sum_DATA)
{
    include 'db/db_conn.php';
    $sql     = "SELECT * FROM product";
    $resultx = $conn->query($sql);
    if ($resultx->num_rows > 0) {
        // output data of each row
        while ($rowO = $resultx->fetch_assoc()) {
            if ($rowO["PRODUCT_NAME"] == $product_name) {
                $S   = $rowO["S"];
                $h   = $rowO["H"];
                $C   = $rowO["C"];
                $MIN = $rowO["MIN"];
            }
        }
    } else {
        echo "0 results";
        exit;
    }
    $conn->close();
    $N = 1;
    for ($i = 0; $i <= 12; $i++) {
        $Demand[$i]               = 0;
        $Quantity[$i]             = 0;
        $Beginning_Inventory[$i]  = 0;
        $Ending_Inventory[$i]     = 0;
        $Average_Inventory[$i]    = 0;
        $Holding_Cost[$i]         = 0;
        $Ordering_Cost[$i]        = 0;
        $sum_Holding_Cost[$i]     = 0;
        $sum_Ordering_Cost[$i]    = 0;
        $sum_Holding_CostEOQ[$i]  = 0;
        $sum_Ordering_CostEOQ[$i] = 0;
    }
    $COUNT_DATA = 1;
    
    ######################################
    
    $Q = ceil(sqrt((2 * $sum_DATA * $S) / (0.2 * $C)));
    
    ######################################
    
    for ($ix = 0; $ix < 12; $ix++) {
        $Y = $rawDatas[$ix];
        if ($COUNT_DATA < 13) {
            
            if ($COUNT_DATA <= 1) {
                $Demand[$COUNT_DATA]     = $Y;
                $order_time[$COUNT_DATA] = round(($Y / $Q), 0);
                if (($Q * $order_time[$COUNT_DATA]) < $Demand[$COUNT_DATA]) {
                    $Quantity[$COUNT_DATA] = ($Q * $order_time[$COUNT_DATA]) + ($Demand[$COUNT_DATA] - ($Q * $order_time[$COUNT_DATA]));
                } else {
                    $Quantity[$COUNT_DATA] = ($Q * $order_time[$COUNT_DATA]);
                }
                $QuantityArray[$COUNT_DATA]       = $Quantity[$COUNT_DATA];
                $Beginning_Inventory[$COUNT_DATA] = $Quantity[$COUNT_DATA];
                $Ending_Inventory[$COUNT_DATA]    = $Beginning_Inventory[$COUNT_DATA] - $Demand[$COUNT_DATA];
                $Average_Inventory[$COUNT_DATA]   = ($Beginning_Inventory[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA]) / 2;
                $Holding_Cost[$COUNT_DATA]        = $Average_Inventory[$COUNT_DATA] * (0.2 / 12) * $C;
                if ($Quantity[$COUNT_DATA] > 0) {
                    $Ordering_Cost[$COUNT_DATA] = round($S * $order_time[$COUNT_DATA], 2);
                } else {
                    $Ordering_Cost[$COUNT_DATA] = 0;
                }
                $sum_Holding_CostEOQ[$COUNT_DATA]  = $sum_Holding_CostEOQ[$COUNT_DATA] + $Holding_Cost[$COUNT_DATA];
                $sum_Ordering_CostEOQ[$COUNT_DATA] = $sum_Ordering_CostEOQ[$COUNT_DATA] + $Ordering_Cost[$COUNT_DATA];
            } else {
                $Demand[$COUNT_DATA]     = $Y;
                $order_time[$COUNT_DATA] = round(($Y / $Q), 0);
                if ($Demand[$COUNT_DATA] > $Ending_Inventory[$COUNT_DATA - 1]) {
                    if (($Q * $order_time[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA - 1]) < $Demand[$COUNT_DATA]) {
                        $Quantity[$COUNT_DATA] = ($Q * $order_time[$COUNT_DATA]) + ($Demand[$COUNT_DATA] - (($Q * $order_time[$COUNT_DATA]) + $Ending_Inventory[$COUNT_DATA - 1]));
                    } else {
                        $Quantity[$COUNT_DATA] = ($Q * $order_time[$COUNT_DATA]);
                    }
                } else {
                    $Quantity[$COUNT_DATA] = 0;
                }
                
                
                $QuantityArray[$COUNT_DATA]       = $Quantity[$COUNT_DATA];
                $Beginning_Inventory[$COUNT_DATA] = $Ending_Inventory[$COUNT_DATA - 1] + $Quantity[$COUNT_DATA];
                $Ending_Inventory[$COUNT_DATA]    = $Beginning_Inventory[$COUNT_DATA] - $Demand[$COUNT_DATA];
                $Average_Inventory[$COUNT_DATA]   = ($Beginning_Inventory[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA]) / 2;
                $Holding_Cost[$COUNT_DATA]        = $Average_Inventory[$COUNT_DATA] * (0.2 / 12) * $C;
                if ($Quantity[$COUNT_DATA] > 0) {
                    $Ordering_Cost[$COUNT_DATA] = round($S * $order_time[$COUNT_DATA], 2);
                } else {
                    $Ordering_Cost[$COUNT_DATA] = 0;
                }
                $sum_Holding_CostEOQ[$COUNT_DATA]  = $sum_Holding_CostEOQ[$COUNT_DATA] + $Holding_Cost[$COUNT_DATA];
                $sum_Ordering_CostEOQ[$COUNT_DATA] = $sum_Ordering_CostEOQ[$COUNT_DATA] + $Ordering_Cost[$COUNT_DATA];
            }
        }
        
        $COUNT_DATA++;
    }
    $Total_CostEOQ         = 0;
    $sum_Ordering_CostEOQX = 0;
    $sum_Holding_CostEOQX  = 0;
    for ($i = 1; $i <= 12; $i++) {
        $Total_CostEOQ         = $Total_CostEOQ + $sum_Ordering_CostEOQ[$i] + $sum_Holding_CostEOQ[$i];
        $sum_Ordering_CostEOQX = $sum_Ordering_CostEOQX + $sum_Ordering_CostEOQ[$i];
        $sum_Holding_CostEOQX  = $sum_Holding_CostEOQX + $sum_Holding_CostEOQ[$i];
    }
    $EOQ_DATA = array(
        1 => $Total_CostEOQ,
        2 => $QuantityArray,
        3 => $Beginning_Inventory,
        4 => $Ending_Inventory,
        5 => $Average_Inventory,
        6 => $Holding_Cost,
        7 => $Ordering_Cost,
        8 => $sum_Holding_CostEOQ,
        9 => $sum_Ordering_CostEOQ,
        10 => $sum_Ordering_CostEOQX,
        11 => $sum_Holding_CostEOQX,
        12 => $rawDatas
    );
    return $EOQ_DATA;
}
/* ################################################################################################### */
function POQ($product_name, $rawDatas, $dBarPOQ, $sum_DATA)
{
    include 'db/db_conn.php';
    $sql     = "SELECT * FROM product";
    $resultx = $conn->query($sql);
    if ($resultx->num_rows > 0) {
        // output data of each row
        while ($rowO = $resultx->fetch_assoc()) {
            if ($rowO["PRODUCT_NAME"] == $product_name) {
                $S   = $rowO["S"];
                $h   = $rowO["H"];
                $C   = $rowO["C"];
                $MIN = $rowO["MIN"];
            }
        }
    } else {
        echo "0 results";
        exit;
    }
    $conn->close();
    for ($i = 0; $i <= 12; $i++) {
        $Demand[$i]               = 0;
        $Quantity[$i]             = 0;
        $Beginning_Inventory[$i]  = 0;
        $Ending_Inventory[$i]     = 0;
        $Average_Inventory[$i]    = 0;
        $Holding_Cost[$i]         = 0;
        $Ordering_Cost[$i]        = 0;
        $sum_Holding_Cost[$i]     = 0;
        $sum_Ordering_Cost[$i]    = 0;
        $sum_Ordering_CostPOQ[$i] = 0;
        $sum_Holding_CostPOQ[$i]  = 0;
        
    }
    $COUNT_DATA = 1;
    
    ########################################
    
    $Q = ceil(sqrt((2 * $sum_DATA * $S) / (0.2 * $C)));
    
    ########################################
    
    for ($ix = 0; $ix < 12; $ix++) {
        $Y = $rawDatas[$ix];
        if ($COUNT_DATA < 13) {
            $Demand[$COUNT_DATA]              = $Y;
            $Quantity[$COUNT_DATA]            = $Demand[$COUNT_DATA];
            $QuantityArray[$COUNT_DATA]       = $Quantity[$COUNT_DATA];
            $Beginning_Inventory[$COUNT_DATA] = $Quantity[$COUNT_DATA];
            $Ending_Inventory[$COUNT_DATA]    = $Beginning_Inventory[$COUNT_DATA] - $Demand[$COUNT_DATA];
            $Average_Inventory[$COUNT_DATA]   = ($Beginning_Inventory[$COUNT_DATA] + $Ending_Inventory[$COUNT_DATA]) / 2;
            $Holding_Cost[$COUNT_DATA]        = round($Average_Inventory[$COUNT_DATA] * (0.2 / 12) * $C, 2);
            if ($Quantity[$COUNT_DATA] > 0) {
                $Ordering_Cost[$COUNT_DATA] = round($S, 2);
            } else {
                $Ordering_Cost[$COUNT_DATA] = 0;
            }
            $sum_Holding_CostPOQ[$COUNT_DATA]  = $sum_Holding_CostPOQ[$COUNT_DATA] + $Holding_Cost[$COUNT_DATA];
            $sum_Ordering_CostPOQ[$COUNT_DATA] = $sum_Ordering_CostPOQ[$COUNT_DATA] + $Ordering_Cost[$COUNT_DATA];
        }
        
        $COUNT_DATA++;
    }
    $Total_CostPOQ         = 0;
    $sum_Ordering_CostPOQX = 0;
    $sum_Holding_CostPOQX  = 0;
    for ($i = 1; $i <= 12; $i++) {
        $Total_CostPOQ         = $Total_CostPOQ + $sum_Ordering_CostPOQ[$i] + $sum_Holding_CostPOQ[$i];
        $sum_Ordering_CostPOQX = $sum_Ordering_CostPOQX + $sum_Ordering_CostPOQ[$i];
        $sum_Holding_CostPOQX  = $sum_Holding_CostPOQX + $sum_Holding_CostPOQ[$i];
    }
    $POQ_DATA = array(
        1 => $Total_CostPOQ,
        2 => $QuantityArray,
        3 => $Beginning_Inventory,
        4 => $Ending_Inventory,
        5 => $Average_Inventory,
        6 => $Holding_Cost,
        7 => $Ordering_Cost,
        8 => $sum_Holding_CostPOQ,
        9 => $sum_Ordering_CostPOQ,
        10 => $sum_Ordering_CostPOQX,
        11 => $sum_Holding_CostPOQX,
        12 => $rawDatas
    );
    return $POQ_DATA;
}
