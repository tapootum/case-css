<?php
session_start();
include '../db/db_conn.php';
if (!$_SESSION["UserID"]){  //check session

	  Header("Location: index.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form

}else{
        $NUMBER=0;
?>
<!doctype html>
<html>
<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="../img/favicon.png" />
    <link rel="stylesheet" href="../build/css-mint.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    #tdw {
    	width: 200px;
    }
    #tds {
    	width: 500px;
    }
    #tdr {
    	width: 200px;
    }
    </style>
</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="../index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="about.php">About</a></li>
                    <li><a href="contact.php">Contact</a></li>
                    <li><a href="admin.php">Admin</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </header><br>

<br>
<br>
<br>


<?php
$target_dir = "../";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
/*if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}*/
// Check if file already exists
/*if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}*/
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "xls" && $imageFileType != "xlsx" ) {
    echo "Sorry, only xls xlsx files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        echo '<br><a  href="admin.php">Back to admin page.</a>';
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>

</body>
</html>
<?php }?>

