<?php
session_start();
include '../db/db_conn.php';
if (!$_SESSION["UserID"]){  //check session

	  Header("Location: index.php"); //ไม่พบผู้ใช้กระโดดกลับไปหน้า login form

}else{?>
<!doctype html>
<html>
<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="../img/favicon.png" />
    <link rel="stylesheet" href="../build/css-mint.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    #tdw {
        width: 10px;
    }
    #tds {
        width: 500px;
    }
    #tdr {
        width: 200px;
    }
    </style>


</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="../index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="../index.php">Home</a></li>
                    <li><a href="../about.php">About</a></li>
                    <li><a href="../contact.php">Contact</a></li>
                    <li><a href="admin.php">Admin</a></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </nav>
        </div>
    </header><br>
<form action="save.php?ID=<?php echo $_GET["ID"];?>" name="frmEdit" method="post">
<table>
    <tr>
        <!--td><center>ID</td-->
        <td><center>ผลิตภัณฑ์</td>
        <td><center>ค่าใช้จ่ายในการสั่งสินค้า<br>(S) (บาท/ครั้ง)</td>
        <td><center>ต้นทุนสินค้าต่อหน่วย<br>(C) (บาท/หน่วย)</td>
        <td><center>ค่าใช้จ่ายในการถือครองสินค้า<br>(H) (บาท/หน่วย)</td>
        <td><center>ระดับสินค้าคงคลังปลอดภัย<br>(หน่วย)</td>
        <td></td>
    </tr>
	<?php
        $sql = 'SELECT * FROM product where ID="'.$_GET["ID"].'"';
	$resultx = $conn->query($sql);
        if ($resultx->num_rows > 0) {
	    // output data of each row
	    while($rowO=$resultx->fetch_assoc()) {
            echo '<tr>';    
            #echo '<td>'.$rowO["ID"].'</td>';       
            #echo '<td width="3%"><input type="text" name="ID" value="'.$rowO["ID"].'" size="4"></td>';       
            echo '<td width="25%"><center><input type="text" name="productName" value="'.$rowO["PRODUCT_NAME"].'" size="40"></td>';       
            echo '<td width="17%"><input  type="text" name="S" value="'.$rowO["S"].'" size="5" ></td>';       
            echo '<td width="17%"><input type="text" name="C" value="'.$rowO["C"].'" size="5"></td>';       
            echo '<td width="17%"><input type="text" name="H" value="'.$rowO["H"].'" size="5"></td>';       
            echo '<td width="17%"><input type="text" name="MIN" value="'.$rowO["MIN"].'" size="10"></td>';       
            echo '<td></td>';
            echo '</tr>';
	    }
        }
        else {
            echo "0 results";
            exit;
        } 
        ?>
    <tr>
        <td colspan=6><input type="submit" name="submit" value="submit"></td>
    </tr>
</table>
</form>

</body>
</html>
<?php }?>

