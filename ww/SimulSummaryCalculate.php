<?php
  require './Const.php';
  require './SimulSummary.php';

  class SimulSummaryCalculate {
    private $simulCalc;

    public function __construct(SimulCalculate $simulCalc) {
      $this->simulCalc = $simulCalc;
    }

    public function calcSummary(SimulInput $simulInput, $simulMins) {
      $qtys = [];
      for ($month = 0; $month < $GLOBALS['MONTH']; $month++) {
        if(empty($qtys[$month])) {
          $qtys[$month] = 0;
        }

        $qtys[$simulMins[$month]->getIndex()] += $simulInput->getDemands()[$month];
      }

      $inventories = $this->simulCalc->calcBeginAndEndInventory($qtys);
      $avgs = $this->simulCalc->calcAvgInventory($inventories[0], $inventories[1]);
      $holdingCosts = $this->simulCalc->calcHoldingCost($simulInput->getC(), $avgs);
      $orderCosts = $this->simulCalc->calcOrderCost($simulInput->getS(), $qtys);

      return new SimulSummary($qtys, $inventories, $avgs, $holdingCosts, $orderCosts);
    }
  }