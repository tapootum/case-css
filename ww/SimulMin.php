<?php
  class SimulMin {
    private $index;
    private $simul;

    public function __construct($index, $simul) {
      $this->index = $index;
      $this->simul = $simul;
    }

    public function getIndex() {
      return $this->index;
    }

    public function getSimul() {
      return $this->simul;
    }
  }