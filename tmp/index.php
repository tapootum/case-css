<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="shortcut icon" href="img/favicon.png" />
    <link rel="stylesheet" href="build/css-mint.min.css">
    <style>
    .grid *[class*='col-span'] > div {
        color: #fff;
        background-color: #5C9DED;
        margin: 5px;
        padding: 7px;
        font-weight: 100%;
    }
    </style>
</head>

<body style="padding: 30px 30px 100%;">
    <header class="header">
        <div class="logo">
            <a href="index.php"><h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1></a>
        </div>
        <div class="nav-right">
            <nav class="navbar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>
            </nav>
        </div>
    </header><br>
    <!-- Header -->
        <!-- Input -->
    <h3 style="margin-top: 50px;"1>เลือกสินค้า</h3>
    <form enctype="multipart/form-data" action="excel-upload.php" method="post" >
    <?php
        session_start();
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "tesis";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        #echo "Connected successfully";
        $sql = "SELECT * from product";
        $result = $conn->query($sql);
    
        echo '<select name="PRODNM">';
    
        if ($result->num_rows > 0) {
    // output data of each row
            while($row = $result->fetch_assoc()) {
                echo '<option value="' . $row["PRODUCT_NAME"].'">'.$row["PRODUCT_NAME"].'</option>';
            }
        } else {
            echo "0 results";
        }
        $conn->close();
        ?>
        <option>Here is the first option</option>
        </select><br><br>
        <h4>ข้อมูลยอดขายจริงรายเดือนย้อนหลัง (.xlsx only)</h4>
        *** จะต้องเป็นข้อมูลยอดขายจริง รายเดือน ย้อนหลัง จำนวน 12-36 เดือนล่าสุด
        <br><br>
        <!--div class="file-upload"-->
            <!--img class="upload-icon" src="http://icons.iconarchive.com/icons/elegantthemes/beautiful-flat/128/upload-icon.png"/-->
            <!--label class="upload-label">Upload file</label-->
            <input class="btn info line" type="file" name="filevc" id="filevc" required />
        <!--/div--><br><br>
        <h4>ข้อมูลความต้องการที่พยากรณ์ไว้ (.xlsx only)</h4>
            <input class="btn info line" type="file" name="file" id="file" required />
        <br><br>
        <input type="submit" value="Submit" />
    </form>
    <!-- Input -->
</body>

</html>