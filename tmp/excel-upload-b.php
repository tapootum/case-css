<!DOCTYPE html>
<html lang="en">

<head>
    <title>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</title>
</head>

<body style="padding: 30px 30px 50px;">
	<h1>ระบบกำหนดวิธีการสั่งซื้อของผลิตภัณฑ์ที่เหมาะสม</h1><br><br>

<?php
	session_start();
	// ini_set('display_errors', 'On');
	// error_reporting(E_ALL | E_STRICT);
	/** Set default timezone (will throw a notice otherwise) */
	// date_default_timezone_set('Asia/Bangkok');
	$PRODUCT_COUNT = 1;
	include 'PHPExcel/IOFactory.php';
	include 'db/db_conn.php';
	include 'function.php';
// include 'menucal.php';
/* ############################# MAIN ############################ */
	if(isset($_FILES['filevc']['name']) && isset($_FILES['file']['name'])){
	    $file_namevc = $_FILES['filevc']['name'];
	    $file_name = $_FILES['file']['name'];
	    $ext = pathinfo($file_namevc, PATHINFO_EXTENSION);
	    $ext2 = pathinfo($file_name, PATHINFO_EXTENSION);
	    if(($ext == "xlsx" || $ext == "xls" || $ext == "ods") && ($ext2 == "xlsx" || $ext2 == "xls" || $ext2 == "ods")){
			$file_namevc = $_FILES['filevc']['tmp_name'];
			$inputFileNamevc = $file_namevc;
			$file_name = $_FILES['file']['tmp_name'];
			$inputFileName = $file_name;

			//  Read your Excel workbook
			try {
				$inputFileTypevc = PHPExcel_IOFactory::identify($inputFileNamevc);
				$objReadervc = PHPExcel_IOFactory::createReader($inputFileTypevc);
				$objPHPExcelvc = $objReadervc->load($inputFileNamevc);

				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReadervc->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileNamevc, PATHINFO_BASENAME). '": ' . $e->getMessage());
			}

			//Table used to display the contents of the file
			
			//  Get worksheet dimensions
			$sheet_ProductNamevc = $objPHPExcelvc->getSheet(0);
			$highestRow_PNvc = $sheet_ProductNamevc->getHighestRow();
			$highestColumn_PNvc = $sheet_ProductNamevc->getHighestColumn();

			$sheet_ProductName = $objPHPExcel->getSheet(0);
			$highestRow_PN = $sheet_ProductName->getHighestRow();
			$highestColumn_PN = $sheet_ProductName->getHighestColumn();

			$_SESSION["highestRow_PN"] = $highestRow_PN;
			$_SESSION["highestColumn_PN"] = $highestColumn_PN;
			//  Loop through each row of the worksheet in turn
			for ($row = 3; $row <= $highestRow_PNvc; $row++) {
				$sum_data1 = 0;
				$sum_data2 = 0;

				//  Read a row of data into an array
				$product_names = $sheet_ProductNamevc->rangeToArray('B' . $row . ':' . 'B' . $row, NULL, TRUE, FALSE);
				$rowData = $sheet_ProductNamevc->rangeToArray('C' . $row . ':' . $highestColumn_PNvc . $row, NULL, TRUE, FALSE);
				//echoing every cell in the selected row for simplicity. You can save the data in database too.
				$MOUNT_COUNT = 0;
				foreach($product_names[0] as $y=>$P){  
					if($P == $_POST["PRODNM"]){
						foreach($rowData[0] as $k=>$v){
							$MOUNT_COUNT++;
							$sum_data1 = $sum_data1+$v;
							$sum_data2 = $sum_data2+($v*$v);
						}
						$product_name_vc = $P;
						$productData = $rowData;
						$dBar_x = dBar($sum_data1,$MOUNT_COUNT);
						$estvard = est_var_d($sum_data1, $sum_data2,$MOUNT_COUNT);
						$vcCal = vc($sum_data1, $sum_data2,$MOUNT_COUNT);
                       
						$_SESSION["product_name_vc"] = $product_name_vc;
						$_SESSION["productData"] = $productData;
						$_SESSION["dBar_x"] = $dBar_x;
						$_SESSION["estvard"] = $estvard;
						$_SESSION["vcCal"] = $vcCal;
					} //end if product_name
				}
			}
			/// FILE 2 
			for ($row = 3; $row <= $highestRow_PN; $row++) {

				//  Read a row of data into an array
				$product_names2 = $sheet_ProductName->rangeToArray('B' . $row . ':' . 'B' . $row, NULL, TRUE, FALSE);
				$rowData2 = $sheet_ProductName->rangeToArray('C' . $row . ':' . $highestColumn_PN . $row, NULL, TRUE, FALSE);
				//echoing every cell in the selected row for simplicity. You can save the data in database too.
				$MOUNT_COUNT2 = 0;
				foreach($product_names2[0] as $y=>$P2){  
					if($P2 == $_POST["PRODNM"]){
						foreach($rowData2[0] as $k=>$v){
						$productData2[$MOUNT_COUNT2] = $v;
						$MOUNT_COUNT2++;
						}
					}
				}
			}

			#echo $_SESSION["productData2"];
			$_SESSION["productData2"] = $productData2;
			
				echo '<br><br><ul class="breadcrumb">
					<li><a href="index.php">หน้าแรก</a></li>
					<li><a href="#">เลือกวิธีการสั่งซื้อ</a></li></ul>';
				
				// echo '<br><br>'.'<a href="index.php">Home</a> > '.'เลือกวิธีการสั่งซื้อ';
				echo '<br>'.'<h4>'."ชื่อสินค้า : ".$_SESSION["product_name_vc"].'</h4>';
				echo '<h4>';
				printf("ค่า Variability Coefficient (VC) เท่ากับ %.2f", round($_SESSION["vcCal"],2));
				echo '</h4>'.'<br>';
				echo "คำแนะนำ : ถ้าค่า VC น้อยกว่าหรือเท่ากับ 0.20 ควรเลือกวิธีการสั่งซื้อเป็น EOQ และ POQ";
				echo '<br>'.'<br>';
				echo "___________________________________________________________________";
				echo '<h4>'."เลือกวิธีการสั่งซื้อ ".'</h4>'."(เลือกได้มากกว่า 1 วิธี)";
				echo '<form action="menucal.php" method="post">';
			    echo '<input type="checkbox" name="CEOQ" value="EOQ"> ทางเลือกที่ 1 วิธีการสั่งซื้อ แบบ EOQ<br>';
			    echo '<input type="checkbox" name="CPOQ" value="POQ"> ทางเลือกที่ 2 วิธีการสั่งซื้อ แบบ POQ<br>';
			    echo '<input type="checkbox" name="CSM" value="SM"> ทางเลือกที่ 3 วิธีการสั่งซื้อ แบบ Silver-Meal<br>';
			    echo '<input type="checkbox" name="CWW" value="WW"> ทางเลือกที่ 4 วิธีการสั่งซื้อ แบบ Wagner-Within<br>';
			    echo '<input type="hidden" name="vcCal" value="'.$_SESSION["vcCal"].'">';
			    echo '<input type="hidden" name="productData" value="'.htmlentities(serialize($_SESSION["productData2"])).'">';
			    echo '<input type="hidden" name="productNamevc" value="'.$_SESSION["product_name_vc"].'">';
			    // echo '<input type="hidden" name="FILESNAME" value="'.$FILES_NAME.'">';
			    echo '<input type="hidden" name="highestRowPN" value="'.$_SESSION["highestRow_PN"].'">';
			    echo '<input type="hidden" name="highestColumnPN" value="'.$_SESSION["highestColumn_PN"].'">';
			    
			  	// echo '<input type="submit" name"submit" value="Submit">';
			  	echo '<br>'.'<button name="name" value="value" type="submit">Submit</button>';
				echo '</form>';
				echo "___________________________________________________________________";
			
		} else {
			echo '<p style="color:red;">Please upload file with xlsx extension only</p>'; 
		}
	} else {
	echo '<br><br><ul class="breadcrumb">
		  	<li><a href="index.php">หน้าแรก</a></li>
		  	<li><a href="#">เลือกวิธีการสั่งซื้อ</a></li></ul>';

    
	// echo '<br><br>'.'<a href="index.php">Home</a> > '.'เลือกวิธีการสั่งซื้อ';
	echo '<br>'.'<h4>'."ชื่อสินค้า : ".$_SESSION["product_name_vc"].'</h4>';
	echo '<h4>';
	printf("ค่า Variability Coefficient (VC) เท่ากับ %.2f", round($_SESSION["vcCal"],2));
	echo '</h4>'.'<br>';
	echo "คำแนะนำ : ถ้าค่า VC น้อยกว่าหรือเท่ากับ 0.20 ควรเลือกวิธีการสั่งซื้อเป็น EOQ และ POQ";
	echo '<br>'.'<br>';
	echo "___________________________________________________________________";
	echo '<h4>'."เลือกวิธีการสั่งซื้อ ".'</h4>'."(เลือกได้มากกว่า 1 วิธี)";
	echo '<form action="menucal.php" method="post">';
    echo '<input type="checkbox" name="CEOQ" value="EOQ"> ทางเลือกที่ 1 วิธีการสั่งซื้อ แบบ EOQ<br>';
    echo '<input type="checkbox" name="CPOQ" value="POQ"> ทางเลือกที่ 2 วิธีการสั่งซื้อ แบบ POQ<br>';
    echo '<input type="checkbox" name="CSM" value="SM"> ทางเลือกที่ 3 วิธีการสั่งซื้อ แบบ Silver-Meal<br>';
    echo '<input type="checkbox" name="CWW" value="WW"> ทางเลือกที่ 4 วิธีการสั่งซื้อ แบบ Wagner-Within<br>';
    echo '<input type="hidden" name="vcCal" value="'.$_SESSION["vcCal"].'">';
    echo '<input type="hidden" name="productData" value="'.htmlentities(serialize($_SESSION["productData2"])).'"">';
    echo '<input type="hidden" name="productNamevc" value="'.$_SESSION["product_name_vc"].'">';
    // echo '<input type="hidden" name="FILESNAME" value="'.$FILES_NAME.'">';
    echo '<input type="hidden" name="highestRowPN" value="'.$_SESSION["highestRow_PN"].'">';
    echo '<input type="hidden" name="highestColumnPN" value="'.$_SESSION["highestColumn_PN"].'">';
    
  	// echo '<input type="submit" name"submit" value="Submit">';
  	echo '<br>'.'<button name="name" value="value" type="submit">Submit</button>';
	echo '</form>';
	echo "___________________________________________________________________";
}
?>

	</body>

</html>